-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 05 Eyl 2016, 12:19:27
-- Sunucu sürümü: 5.6.17
-- PHP Sürümü: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Veritabanı: `pokemon_world`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `appearance`
--

CREATE TABLE IF NOT EXISTS `appearance` (
  `pokemon_name` varchar(20) NOT NULL DEFAULT '',
  `region_name` varchar(20) NOT NULL DEFAULT '',
  `probability` double DEFAULT NULL,
  PRIMARY KEY (`pokemon_name`,`region_name`),
  KEY `region_name` (`region_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `canbeat`
--

CREATE TABLE IF NOT EXISTS `canbeat` (
  `type1` varchar(20) NOT NULL DEFAULT '',
  `type2` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`type1`,`type2`),
  KEY `type2` (`type2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tablo döküm verisi `canbeat`
--

INSERT INTO `canbeat` (`type1`, `type2`) VALUES
('Fire', 'Bug'),
('Flying', 'Bug'),
('Rock', 'Bug'),
('Steel', 'Bug'),
('Bug', 'Dark'),
('Fairy', 'Dark'),
('Fighting', 'Dark'),
('Dragon', 'Dragon'),
('Fairy', 'Dragon'),
('Ice', 'Dragon'),
('Ground', 'Electric'),
('Poison', 'Fairy'),
('Steel', 'Fairy'),
('Fairy', 'Fighting'),
('Flying', 'Fighting'),
('Psychic', 'Fighting'),
('Ground', 'Fire'),
('Rock', 'Fire'),
('Water', 'Fire'),
('Electric', 'Flying'),
('Ice', 'Flying'),
('Rock', 'Flying'),
('Dark', 'Ghost'),
('Ghost', 'Ghost'),
('Bug', 'Grass'),
('Fire', 'Grass'),
('Flying', 'Grass'),
('Ice', 'Grass'),
('Poison', 'Grass'),
('Steel', 'Grass'),
('Grass', 'Ground'),
('Ice', 'Ground'),
('Water', 'Ground'),
('Fighting', 'Ice'),
('Fire', 'Ice'),
('Rock', 'Ice'),
('Fighting', 'Normal'),
('Ground', 'Poison'),
('Psychic', 'Poison'),
('Bug', 'Psychic'),
('Dark', 'Psychic'),
('Ghost', 'Psychic'),
('Fighting', 'Rock'),
('Grass', 'Rock'),
('Ground', 'Rock'),
('Water', 'Rock'),
('Fighting', 'Steel'),
('Fire', 'Steel'),
('Ground', 'Steel'),
('Electric', 'Water'),
('Grass', 'Water');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `evolution`
--

CREATE TABLE IF NOT EXISTS `evolution` (
  `stage1` varchar(20) NOT NULL DEFAULT '',
  `stage2` varchar(20) DEFAULT NULL,
  `level_` int(11) DEFAULT NULL,
  PRIMARY KEY (`stage1`),
  KEY `stage2` (`stage2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `isimmune`
--

CREATE TABLE IF NOT EXISTS `isimmune` (
  `type1` varchar(20) NOT NULL DEFAULT '',
  `type2` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`type1`,`type2`),
  KEY `type2` (`type2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tablo döküm verisi `isimmune`
--

INSERT INTO `isimmune` (`type1`, `type2`) VALUES
('Psychic', 'Dark'),
('Dragon', 'Fairy'),
('Ground', 'Flying'),
('Fighting', 'Ghost'),
('Normal', 'Ghost'),
('Electric', 'Ground'),
('Ghost', 'Normal'),
('Poison', 'Steel');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `istype`
--

CREATE TABLE IF NOT EXISTS `istype` (
  `pokemon_name` varchar(20) NOT NULL DEFAULT '',
  `type1` varchar(20) DEFAULT NULL,
  `type2` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`pokemon_name`),
  KEY `type1` (`type1`),
  KEY `type2` (`type2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tablo döküm verisi `istype`
--

INSERT INTO `istype` (`pokemon_name`, `type1`, `type2`) VALUES
('Abra', 'Psychic', NULL),
('Aerodactyl', 'Rock', 'Flying'),
('Alakazam', 'Psychic', NULL),
('Arbok', 'Poison', NULL),
('Arcanine', 'Fire', NULL),
('Articuno', 'Ice', 'Flying'),
('Beedrill', 'Bug', 'Poison'),
('Bellsprout', 'Grass', 'Poison'),
('Blastoise', 'Water', NULL),
('Bulbasaur', 'Grass', 'Poison'),
('Butterfree', 'Bug', 'Flying'),
('Caterpie', 'Bug', NULL),
('Chansey', 'Normal', NULL),
('Charizard', 'Fire', 'Flying'),
('Charmander', 'Fire', NULL),
('Charmeleon', 'Fire', NULL),
('Clefable', 'Fairy', NULL),
('Clefairy', 'Fairy', NULL),
('Cloyster', 'Water', 'Ice'),
('Cubone', 'Ground', NULL),
('Dewgong', 'Water', 'Ice'),
('Diglett', 'Ground', NULL),
('Ditto', 'Normal', NULL),
('Dodrio', 'Normal', 'Flying'),
('Doduo', 'Normal', 'Flying'),
('Dragonair', 'Dragon', NULL),
('Dragonite', 'Dragon', 'Flying'),
('Dratini', 'Dragon', NULL),
('Drowzee', 'Psychic', NULL),
('Dugtrio', 'Ground', NULL),
('Eevee', 'Normal', NULL),
('Ekans', 'Poison', NULL),
('Electabuzz', 'Electric', NULL),
('Electrode', 'Electric', NULL),
('Exeggcute', 'Grass', 'Psychic'),
('Exeggutor', 'Grass', 'Psychic'),
('Farfetch''d', 'Normal', 'Flying'),
('Fearow', 'Normal', 'Flying'),
('Flareon', 'Fire', NULL),
('Gastly', 'Ghost', 'Poison'),
('Gengar', 'Ghost', 'Poison'),
('Geodude', 'Rock', 'Ground'),
('Gloom', 'Grass', 'Poison'),
('Golbat', 'Poison', 'Flying'),
('Goldeen', 'Water', NULL),
('Golduck', 'Water', NULL),
('Golem', 'Rock', 'Ground'),
('Graveler', 'Rock', 'Ground'),
('Grimer', 'Poison', NULL),
('Growlithe', 'Fire', NULL),
('Gyarados', 'Water', 'Flying'),
('Haunter', 'Ghost', 'Poison'),
('Hitmonchan', 'Fighting', NULL),
('Hitmonlee', 'Fighting', NULL),
('Horsea', 'Water', NULL),
('Hypno', 'Psychic', NULL),
('Ivysaur', 'Grass', 'Poison'),
('Jigglypuff', 'Normal', 'Fairy'),
('Jolteon', 'Electric', NULL),
('Jynx', 'Ice', 'Psychic'),
('Kabuto', 'Rock', 'Water'),
('Kabutops', 'Rock', 'Water'),
('Kadabra', 'Psychic', NULL),
('Kakuna', 'Bug', 'Poison'),
('Kangaskhan', 'Normal', NULL),
('Kingler', 'Water', NULL),
('Koffing', 'Poison', NULL),
('Krabby', 'Water', NULL),
('Lapras', 'Water', 'Ice'),
('Lickitung', 'Normal', NULL),
('Machamp', 'Fighting', NULL),
('Machoke', 'Fighting', NULL),
('Machop', 'Fighting', NULL),
('Magikarp', 'Water', NULL),
('Magmar', 'Fire', NULL),
('Magnemite', 'Electric', 'Steel'),
('Magneton', 'Electric', 'Steel'),
('Mankey', 'Fighting', NULL),
('Marowak', 'Ground', NULL),
('Meowth', 'Normal', NULL),
('Metapod', 'Bug', NULL),
('Mew', 'Psychic', NULL),
('Mewtwo', 'Psychic', NULL),
('Moltres', 'Fire', 'Flying'),
('Mr. Mime', 'Psychic', 'Fairy'),
('Muk', 'Poison', NULL),
('Nidoking', 'Poison', 'Ground'),
('Nidoqueen', 'Poison', 'Ground'),
('Nidoran-f', 'Poison', NULL),
('Nidoran-m', 'Poison', NULL),
('Nidorina', 'Poison', NULL),
('Nidorino', 'Poison', NULL),
('Ninetales', 'Fire', NULL),
('Oddish', 'Grass', 'Poison'),
('Omanyte', 'Rock', 'Water'),
('Omastar', 'Rock', 'Water'),
('Onix', 'Rock', 'Ground'),
('Paras', 'Bug', 'Grass'),
('Parasect', 'Bug', 'Grass'),
('Persian', 'Normal', NULL),
('Pidgeot', 'Normal', 'Flying'),
('Pidgeotto', 'Normal', 'Flying'),
('Pidgey', 'Normal', 'Flying'),
('Pikachu', 'Electric', NULL),
('Pinsir', 'Bug', NULL),
('Poliwag', 'Water', NULL),
('Poliwhirl', 'Water', NULL),
('Poliwrath', 'Water', 'Fighting'),
('Ponyta', 'Fire', NULL),
('Porygon', 'Normal', NULL),
('Primeape', 'Fighting', NULL),
('Psyduck', 'Water', NULL),
('Raichu', 'Electric', NULL),
('Rapidash', 'Fire', NULL),
('Raticate', 'Normal', NULL),
('Rattata', 'Normal', NULL),
('Rhydon', 'Ground', 'Rock'),
('Rhyhorn', 'Ground', 'Rock'),
('Sandshrew', 'Ground', NULL),
('Sandslash', 'Ground', NULL),
('Scyther', 'Bug', 'Flying'),
('Seadra', 'Water', NULL),
('Seaking', 'Water', NULL),
('Seel', 'Water', NULL),
('Shellder', 'Water', NULL),
('Slowbro', 'Water', 'Psychic'),
('Slowpoke', 'Water', 'Psychic'),
('Snorlax', 'Normal', NULL),
('Spearow', 'Normal', 'Flying'),
('Squirtle', 'Water', NULL),
('Starmie', 'Water', 'Psychic'),
('Staryu', 'Water', NULL),
('Tangela', 'Grass', NULL),
('Tauros', 'Normal', NULL),
('Tentacool', 'Water', 'Poison'),
('Tentacruel', 'Water', 'Poison'),
('Vaporeon', 'Water', NULL),
('Venomoth', 'Bug', 'Poison'),
('Venonat', 'Bug', 'Poison'),
('Venusaur', 'Grass', 'Poison'),
('Victreebel', 'Grass', 'Poison'),
('Vileplume', 'Grass', 'Poison'),
('Voltorb', 'Electric', NULL),
('Vulpix', 'Fire', NULL),
('Wartortle', 'Water', NULL),
('Weedle', 'Bug', 'Poison'),
('Weepinbell', 'Grass', 'Poison'),
('Weezing', 'Poison', NULL),
('Wigglytuff', 'Normal', 'Fairy'),
('Zapdos', 'Electric', 'Flying'),
('Zubat', 'Poison', 'Flying');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `pokemonid`
--

CREATE TABLE IF NOT EXISTS `pokemonid` (
  `pid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `pokemoninfo`
--

CREATE TABLE IF NOT EXISTS `pokemoninfo` (
  `name` varchar(20) NOT NULL DEFAULT '',
  `hp` int(11) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `defence` int(11) DEFAULT NULL,
  `special_attack` int(11) DEFAULT NULL,
  `special_defense` int(11) DEFAULT NULL,
  `speed` int(11) DEFAULT NULL,
  `legendary` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tablo döküm verisi `pokemoninfo`
--

INSERT INTO `pokemoninfo` (`name`, `hp`, `attack`, `defence`, `special_attack`, `special_defense`, `speed`, `legendary`) VALUES
('Abra', 25, 20, 15, 105, 55, 90, 0),
('Aerodactyl', 80, 105, 65, 60, 75, 130, 0),
('Alakazam', 55, 50, 45, 135, 95, 120, 0),
('Arbok', 60, 85, 69, 65, 79, 80, 0),
('Arcanine', 90, 110, 80, 100, 80, 95, 0),
('Articuno', 90, 85, 100, 95, 125, 85, 0),
('Beedrill', 65, 90, 40, 45, 80, 75, 0),
('Bellsprout', 50, 75, 35, 70, 30, 40, 0),
('Blastoise', 79, 83, 100, 85, 105, 78, 0),
('Bulbasaur', 45, 49, 49, 65, 65, 45, 0),
('Butterfree', 60, 45, 50, 90, 80, 70, 0),
('Caterpie', 45, 30, 35, 20, 20, 45, 0),
('Chansey', 250, 5, 5, 35, 105, 50, 0),
('Charizard', 78, 84, 78, 109, 85, 100, 0),
('Charmander', 39, 52, 43, 60, 50, 65, 0),
('Charmeleon', 58, 64, 58, 80, 65, 80, 0),
('Clefable', 95, 70, 73, 95, 90, 60, 0),
('Clefairy', 70, 45, 48, 60, 65, 35, 0),
('Cloyster', 50, 95, 180, 85, 45, 70, 0),
('Cubone', 50, 50, 95, 40, 50, 35, 0),
('Dewgong', 90, 70, 80, 70, 95, 70, 0),
('Diglett', 10, 55, 25, 35, 45, 95, 0),
('Ditto', 48, 48, 48, 48, 48, 48, 0),
('Dodrio', 60, 110, 70, 60, 60, 100, 0),
('Doduo', 35, 85, 45, 35, 35, 75, 0),
('Dragonair', 61, 84, 65, 70, 70, 70, 0),
('Dragonite', 91, 134, 95, 100, 100, 80, 0),
('Dratini', 41, 64, 45, 50, 50, 50, 0),
('Drowzee', 60, 48, 45, 43, 90, 42, 0),
('Dugtrio', 35, 80, 50, 50, 70, 120, 0),
('Eevee', 55, 55, 50, 45, 65, 55, 0),
('Ekans', 35, 60, 44, 40, 54, 55, 0),
('Electabuzz', 65, 83, 57, 95, 85, 105, 0),
('Electrode', 60, 50, 70, 80, 80, 140, 0),
('Exeggcute', 60, 40, 80, 60, 45, 40, 0),
('Exeggutor', 95, 95, 85, 125, 65, 55, 0),
('Farfetch''d', 52, 65, 55, 58, 62, 60, 0),
('Fearow', 65, 90, 65, 61, 61, 100, 0),
('Flareon', 65, 130, 60, 95, 110, 65, 0),
('Gastly', 30, 35, 30, 100, 35, 80, 0),
('Gengar', 60, 65, 60, 130, 75, 110, 0),
('Geodude', 40, 80, 100, 30, 30, 20, 0),
('Gloom', 60, 65, 70, 85, 75, 40, 0),
('Golbat', 75, 80, 70, 65, 75, 90, 0),
('Goldeen', 45, 67, 60, 35, 50, 63, 0),
('Golduck', 80, 82, 78, 95, 80, 85, 0),
('Golem', 80, 120, 130, 55, 65, 45, 0),
('Graveler', 55, 95, 115, 45, 45, 35, 0),
('Grimer', 80, 80, 50, 40, 50, 25, 0),
('Growlithe', 55, 70, 45, 70, 50, 60, 0),
('Gyarados', 95, 125, 79, 60, 100, 81, 0),
('Haunter', 45, 50, 45, 115, 55, 95, 0),
('Hitmonchan', 50, 105, 79, 35, 110, 76, 0),
('Hitmonlee', 50, 120, 53, 35, 110, 87, 0),
('Horsea', 30, 40, 70, 70, 25, 60, 0),
('Hypno', 85, 73, 70, 73, 115, 67, 0),
('Ivysaur', 60, 62, 63, 80, 80, 60, 0),
('Jigglypuff', 115, 45, 20, 45, 25, 20, 0),
('Jolteon', 65, 65, 60, 110, 95, 130, 0),
('Jynx', 65, 50, 35, 115, 95, 95, 0),
('Kabuto', 30, 80, 90, 55, 45, 55, 0),
('Kabutops', 60, 115, 105, 65, 70, 80, 0),
('Kadabra', 40, 35, 30, 120, 70, 105, 0),
('Kakuna', 45, 25, 50, 25, 25, 35, 0),
('Kangaskhan', 105, 95, 80, 40, 80, 90, 0),
('Kingler', 55, 130, 115, 50, 50, 75, 0),
('Koffing', 40, 65, 95, 60, 45, 35, 0),
('Krabby', 30, 105, 90, 25, 25, 50, 0),
('Lapras', 130, 85, 80, 85, 95, 60, 0),
('Lickitung', 90, 55, 75, 60, 75, 30, 0),
('Machamp', 90, 130, 80, 65, 85, 55, 0),
('Machoke', 80, 100, 70, 50, 60, 45, 0),
('Machop', 70, 80, 50, 35, 35, 35, 0),
('Magikarp', 20, 10, 55, 15, 20, 80, 0),
('Magmar', 65, 95, 57, 100, 85, 93, 0),
('Magnemite', 25, 35, 70, 95, 55, 45, 0),
('Magneton', 50, 60, 95, 120, 70, 70, 0),
('Mankey', 40, 80, 35, 35, 45, 70, 0),
('Marowak', 60, 80, 110, 50, 80, 45, 0),
('Meowth', 40, 45, 35, 40, 40, 90, 0),
('Metapod', 50, 20, 55, 25, 25, 30, 0),
('Mew', 100, 100, 100, 100, 100, 100, 0),
('Mewtwo', 106, 110, 90, 154, 90, 130, 0),
('Moltres', 90, 100, 90, 125, 85, 90, 0),
('Mr. Mime', 40, 45, 65, 100, 120, 90, 0),
('Muk', 105, 105, 75, 65, 100, 50, 0),
('Nidoking', 81, 102, 77, 85, 75, 85, 0),
('Nidoqueen', 90, 92, 87, 75, 85, 76, 0),
('Nidoran-f', 46, 57, 40, 40, 40, 50, 0),
('Nidoran-m', 55, 47, 52, 40, 40, 41, 0),
('Nidorina', 70, 62, 67, 55, 55, 56, 0),
('Nidorino', 61, 72, 57, 55, 55, 65, 0),
('Ninetales', 73, 76, 75, 81, 100, 100, 0),
('Oddish', 45, 50, 55, 75, 65, 30, 0),
('Omanyte', 35, 40, 100, 90, 55, 35, 0),
('Omastar', 70, 60, 125, 115, 70, 55, 0),
('Onix', 35, 45, 160, 30, 45, 70, 0),
('Paras', 35, 70, 55, 45, 55, 25, 0),
('Parasect', 60, 95, 80, 60, 80, 30, 0),
('Persian', 65, 70, 60, 65, 65, 115, 0),
('Pidgeot', 83, 80, 75, 70, 70, 101, 0),
('Pidgeotto', 63, 60, 55, 50, 50, 71, 0),
('Pidgey', 40, 45, 40, 35, 35, 56, 0),
('Pikachu', 35, 55, 40, 50, 50, 90, 0),
('Pinsir', 65, 125, 100, 55, 70, 85, 0),
('Poliwag', 40, 50, 40, 40, 40, 90, 0),
('Poliwhirl', 65, 65, 65, 50, 50, 90, 0),
('Poliwrath', 90, 95, 95, 70, 90, 70, 0),
('Ponyta', 50, 85, 55, 65, 65, 90, 0),
('Porygon', 65, 60, 70, 85, 75, 40, 0),
('Primeape', 65, 105, 60, 60, 70, 95, 0),
('Psyduck', 50, 52, 48, 65, 50, 55, 0),
('Raichu', 60, 90, 55, 90, 80, 110, 0),
('Rapidash', 65, 100, 70, 80, 80, 105, 0),
('Raticate', 55, 81, 60, 50, 70, 97, 0),
('Rattata', 30, 56, 35, 25, 35, 72, 0),
('Rhydon', 105, 130, 120, 45, 45, 40, 0),
('Rhyhorn', 80, 85, 95, 30, 30, 25, 0),
('Sandshrew', 50, 75, 85, 20, 30, 40, 0),
('Sandslash', 75, 100, 110, 45, 55, 65, 0),
('Scyther', 70, 110, 80, 55, 80, 105, 0),
('Seadra', 55, 65, 95, 95, 45, 85, 0),
('Seaking', 80, 92, 65, 65, 80, 68, 0),
('Seel', 65, 45, 55, 45, 70, 45, 0),
('Shellder', 30, 65, 100, 45, 25, 40, 0),
('Slowbro', 95, 75, 110, 100, 80, 30, 0),
('Slowpoke', 90, 65, 65, 40, 40, 15, 0),
('Snorlax', 160, 110, 65, 65, 110, 30, 0),
('Spearow', 40, 60, 30, 31, 31, 70, 0),
('Squirtle', 44, 48, 65, 50, 64, 43, 0),
('Starmie', 60, 75, 85, 100, 85, 115, 0),
('Staryu', 30, 45, 55, 70, 55, 85, 0),
('Tangela', 65, 55, 115, 100, 40, 60, 0),
('Tauros', 75, 100, 95, 40, 70, 110, 0),
('Tentacool', 40, 40, 35, 50, 100, 70, 0),
('Tentacruel', 80, 70, 65, 80, 120, 100, 0),
('Vaporeon', 130, 65, 60, 110, 95, 65, 0),
('Venomoth', 70, 65, 60, 90, 75, 90, 0),
('Venonat', 60, 55, 50, 40, 55, 45, 0),
('Venusaur', 80, 82, 83, 100, 100, 80, 0),
('Victreebel', 80, 105, 65, 100, 70, 70, 0),
('Vileplume', 75, 80, 85, 110, 90, 50, 0),
('Voltorb', 40, 30, 50, 55, 55, 100, 0),
('Vulpix', 38, 41, 40, 50, 65, 65, 0),
('Wartortle', 59, 63, 80, 65, 80, 58, 0),
('Weedle', 40, 35, 30, 20, 20, 50, 0),
('Weepinbell', 65, 90, 50, 85, 45, 55, 0),
('Weezing', 65, 90, 120, 85, 70, 60, 0),
('Wigglytuff', 140, 70, 45, 85, 50, 45, 0),
('Zapdos', 90, 90, 85, 125, 90, 100, 0),
('Zubat', 40, 45, 35, 30, 40, 55, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `name` varchar(20) NOT NULL DEFAULT '',
  `level_first` int(11) DEFAULT NULL,
  `level_second` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `trains`
--

CREATE TABLE IF NOT EXISTS `trains` (
  `username` varchar(20) DEFAULT NULL,
  `pid` int(11) NOT NULL DEFAULT '0',
  `pokemonexp` int(11) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `name` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tablo döküm verisi `types`
--

INSERT INTO `types` (`name`) VALUES
('Bug'),
('Dark'),
('Dragon'),
('Electric'),
('Fairy'),
('Fighting'),
('Fire'),
('Flying'),
('Ghost'),
('Grass'),
('Ground'),
('Ice'),
('Normal'),
('Poison'),
('Psychic'),
('Rock'),
('Steel'),
('Water');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(20) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `fav_poke` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `appearance`
--
ALTER TABLE `appearance`
  ADD CONSTRAINT `appearance_ibfk_1` FOREIGN KEY (`pokemon_name`) REFERENCES `pokemoninfo` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `appearance_ibfk_2` FOREIGN KEY (`region_name`) REFERENCES `region` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `canbeat`
--
ALTER TABLE `canbeat`
  ADD CONSTRAINT `canbeat_ibfk_1` FOREIGN KEY (`type1`) REFERENCES `types` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `canbeat_ibfk_2` FOREIGN KEY (`type2`) REFERENCES `types` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `evolution`
--
ALTER TABLE `evolution`
  ADD CONSTRAINT `evolution_ibfk_1` FOREIGN KEY (`stage1`) REFERENCES `pokemoninfo` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `evolution_ibfk_2` FOREIGN KEY (`stage2`) REFERENCES `pokemoninfo` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `isimmune`
--
ALTER TABLE `isimmune`
  ADD CONSTRAINT `isimmune_ibfk_1` FOREIGN KEY (`type1`) REFERENCES `types` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `isimmune_ibfk_2` FOREIGN KEY (`type2`) REFERENCES `types` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `istype`
--
ALTER TABLE `istype`
  ADD CONSTRAINT `istype_ibfk_1` FOREIGN KEY (`pokemon_name`) REFERENCES `pokemoninfo` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `istype_ibfk_2` FOREIGN KEY (`type1`) REFERENCES `types` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `istype_ibfk_3` FOREIGN KEY (`type2`) REFERENCES `types` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `pokemonid`
--
ALTER TABLE `pokemonid`
  ADD CONSTRAINT `pokemonid_ibfk_1` FOREIGN KEY (`name`) REFERENCES `pokemoninfo` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `trains`
--
ALTER TABLE `trains`
  ADD CONSTRAINT `trains_ibfk_1` FOREIGN KEY (`username`) REFERENCES `user_info` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trains_ibfk_2` FOREIGN KEY (`pid`) REFERENCES `pokemonid` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
